package com.tac.media.audioplayer;

import java.nio.ByteBuffer;

/**
 * Created by kulik on 22.09.14.
 */
public interface IKCodec {

    public int getReadBufferLength();

    ByteBuffer encode(byte[] data);

    void init();
}

